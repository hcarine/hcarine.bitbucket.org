teste.controller( 'readController' , ['$scope', '$routeParams', '$location',
	function($scope, $routeParams,$location){

	function init(){
		$scope.carSelected = {index:0};
		getCars();
		var checked=0;
		initSlice()
	}

	var MAX_LENGTH = 5;

	$scope.remove = function(carSelected){
		getCars();
		removeItem(carSelected);
		$scope.removeAll();
		addCars();
		getCars();
	}

	$scope.edit =  function(index){
		var link= '/edit/'+ index;
		$location.path(link);
	}

	$scope.removeAll =  function(){
		localStorage.clear();
		getCars();
	}

	$scope.next = function(){
		if(validNext()){
			$scope.page +=1;	
			$scope.end = $scope.page * MAX_LENGTH;
			$scope.init = $scope.end - MAX_LENGTH;
		}
	}

	$scope.previous = function(){
		if(validPrevious()){
			$scope.page -=1;
			$scope.end = $scope.page * MAX_LENGTH;
			$scope.init = $scope.end - MAX_LENGTH;
		}
	}

	function validNext(){
		return $scope.end < $scope.listCar.length;
	}

	function validPrevious(){
		value = $scope.init - MAX_LENGTH;
		return value >= 0;
	}

	function initSlice(){
		$scope.page=1;
		$scope.init = 0;
		$scope.end = MAX_LENGTH;
	}

	function getCars(){
		var cars = localStorage.getItem('cars');
		if( cars != 'undefined')
			return $scope.listCar = JSON.parse(cars);
		return	$scope.listCar = [];
	}

	function removeItem(carSelected){
		$scope.listCar.splice( carSelected, 1 );
		$scope.newListCar = $scope.listCar;
	}

	function addCars(){
		localStorage.setItem('cars',JSON.stringify( $scope.newListCar ));
	}

	function selected(){
		return filterFilter($scope.cars, { selected: true });
	}

	init();

}]);