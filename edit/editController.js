teste.controller( 'editController' , ['$scope', '$routeParams','$location',
 function($scope, $routeParams, $location) {

var index = null;

 	function init(){
 		getCar();
 	}

 	$scope.cancel = function cancelEdit(){
 		goReadCar();
 	}

 	$scope.save = function saveEdit(){
 		if(!$scope.formCar.$valid)
			return message('Preencha campos obrigatórios');
 		$scope.listCars[index]= $scope.car;
		addEdit($scope.listCars);
		goReadCar();
 	}

 	function getCar(){
 		index = getindex();
 		$scope.listCars= JSON.parse(localStorage.getItem('cars'));
 		$scope.car = $scope.listCars[index];
 	}

 	function getindex(){
 		return $routeParams.id;
 	}

	function message(message){
		return alert(message);
	}

	function addEdit(item){
 		localStorage.setItem('cars',JSON.stringify(item));
 	}

 	function goReadCar(){
 		$location.path('/read');
 	}

 	init();

 }]);