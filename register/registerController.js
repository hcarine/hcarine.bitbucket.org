teste.controller( 'registerController' , ['$scope', '$routeParams',
 function($scope, $routeParams) {
 	
 	function init(){
 		getCars();
 	}

  	var defaultForm = {
  		'car.fuel' : "",
		'car.image' : "",
		'car.mark' : "",
		'car.model' : "",
		'car.id' : ""
  	}

	$scope.addCar = function(){
		if(!$scope.formCar.$valid)
			return message('Preencha campos obrigatórios');
		if($scope.car.image === undefined)
			addMarkImage();

		$scope.cars.push({
			fuel : $scope.car.fuel,
			image : $scope.car.image,
			mark : $scope.car.mark,
			model : $scope.car.model,
			id : $scope.car.id
			});
		addItem($scope.cars);
		resetForm();
	}

	$scope.addDBasicBase = function(){
		base_cars = [{
			"fuel" : "Flex",
		    "image" : null,
		    "mark" : "Volkswagem",
		    "model" : "Gol",
		    "id" : "FFF-5498"
		  },
		  { "fuel" : "Gasolina",
		    "image" : null,
		    "mark" : "Volkswagem",
		    "model" : "Fox",
		    "id" : "FOX-4125"
		  },
		  { "fuel" : "Alcool",
		    "image" : "https://lh4.googleusercontent.com/-_AhcQKHf7rM/AAAAAAAAAAI/AAAAAAAAAAA/QM-pqL4NYaE/s48-c-k-no/photo.jpg",
		    "mark" : "Volkswagen",
		    "model" : "Fusca",
		    "id" : "PAI-4121"
		  }];
		addItem(base_cars);
	}

	function message(message){
		return alert(message);
	}

	function addItem(item){
		localStorage.setItem('cars',JSON.stringify(item));
	}

	function getCars(){
		var cars = localStorage.getItem('cars');
		if( cars!='undefined')
			return $scope.cars = JSON.parse(cars);
		return	$scope.cars = [];
	}

	function addMarkImage(){
		 var mark = $scope.car.mark.toLowerCase();
		switch(mark) {
		    case 'chevrolet':
		        $scope.car.image = "http://www.wlelis.com/imagens/automoveis/chevrolet%20logo.jpg";
		        break;
		    case 'ford':
		    	$scope.car.image = "http://www.vilhena.net.br/images/Logos/ford_logo.bmp";
		        break;
		    case 'fiat':
		    	$scope.car.image = "https://upload.wikimedia.org/wikipedia/pt/7/70/Fiat_logo.jpg";
		        break;
		    case 'honda':
		    	$scope.car.image = "http://theautoz.com/images/honda-logo-transparent-background-6.jpg";
		        break;
		    case 'volkswagen':
		    	$scope.car.image = "http://img1.wikia.nocookie.net/__cb20131206125409/logopedia/images/9/9f/Volkswagen56.png";
		        break;
		    case 'hyundai':
		    	$scope.car.image = "http://www.carlogos.org/uploads/carlogos/hyundai-logo-1.jpg";
		        break;
		}
	}	

	function resetForm(){
		$scope.car = angular.copy(defaultForm);
	}


	init();

}]);