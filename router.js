var teste = angular.module('teste', ['ngRoute']);  

teste.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
        .when('/register', {
            templateUrl: 'register/register.html',
            controller: 'registerController'           
        })
        .when('/read',{
            templateUrl: 'read/read.html',
            controller: 'readController'
        })
        .when('/edit/:id',{
            templateUrl: 'edit/edit.html',
            controller: 'editController'
        })
        .otherwise({
            redirectTo: '/'
        });
    }]);

